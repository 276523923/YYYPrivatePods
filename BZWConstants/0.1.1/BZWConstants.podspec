
Pod::Spec.new do |s|
  s.name             = 'BZWConstants'
  s.version          = '0.1.1'
  s.summary          = 'BZWConstants 常量'

  s.description      = <<-DESC

    BZWConstants
    一些最基础的常量跟宏

                       DESC

  s.homepage         = 'https://yyyue@bitbucket.org/yyyue/bzwconstants.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }
  s.source           = { :git => 'https://yyyue@bitbucket.org/yyyue/bzwconstants.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'BZWConstants/Classes/**/*'
  
  # s.resource_bundles = {
  #   'BZWConstants' => ['BZWConstants/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
