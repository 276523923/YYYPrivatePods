#
# Be sure to run `pod lib lint BZWMediator.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'BZWMediator'
  s.version      = '1.0.3'
  s.summary      = 'BZWMediator'

  s.description  = <<-DESC
                    this is BZWMediator
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/BZWMediator.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/BZWMediator.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  # s.static_framework = true

  s.source_files  = "BZWMediator/Classes/**/*.{h,m}"
  s.public_header_files = "BZWMediator/Classes/**/*.h"

  # s.resources = "BZWMediator/Assets/**/*"
  # ss.resource_bundles = {
  #   BZWMediator => ["BZWMediator/Assets/**/*"]
  # }

  s.dependency "YYYTool/Helper/YYYMesssageSend"
  s.dependency "YYYTool/Category/UIKit/UIWindow"


end
