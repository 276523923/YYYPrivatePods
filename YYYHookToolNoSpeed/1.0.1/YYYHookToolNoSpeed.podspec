#
# Be sure to run `pod lib lint YYYHookToolNoSpeed.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'YYYHookToolNoSpeed'
  s.version      = '1.0.1'
  s.summary      = 'YYYHookToolNoSpeed'

  s.description  = <<-DESC
                    this is YYYHookToolNoSpeed
                   DESC

  s.homepage         = 'https://git.code.tencent.com/yyyhook/YYYHookToolNoSpeed.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'https://git.code.tencent.com/yyyhook/YYYHookToolNoSpeed.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  # s.static_framework = true

  s.source_files  = "YYYHookToolNoSpeed/Classes/**/*.{h,m}"
  s.public_header_files = "YYYHookToolNoSpeed/Classes/**/*.h"
  s.vendored_frameworks = "YYYHookToolNoSpeed/Classes/YYYTool.framework"
  # s.resources = "YYYHookToolNoSpeed/Assets/**/*"
  # ss.resource_bundles = {
  #   YYYHookToolNoSpeed => ["YYYHookToolNoSpeed/Assets/**/*"]
  # }

  # s.dependency ""
end
