Pod::Spec.new do |s|
  s.name             = 'BZWWebViewController'
  s.version          = '0.1.0'
  s.summary          = 'BZWWebViewController'
  s.description      = <<-DESC
    BZWWebViewController 网页浏览器
    pod 'BZWWebViewController'

                       DESC

  s.homepage         = 'https://yyyue@bitbucket.org/yyyue/bzwwebviewcontroller.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }
  s.source           = { :git => 'https://yyyue@bitbucket.org/yyyue/bzwwebviewcontroller.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  s.static_framework = true

  s.source_files = 'BZWWebViewController/Classes/**/*'
  s.public_header_files = 'BZWWebViewController/Classes/**/*.h'

  s.dependency 'YYYTool/Helper/YYYCommonMacro'
  s.dependency 'BZWBase/Helper/BZWShare'
  s.dependency 'BZWBase/Helper/BZWConstants'
  s.dependency 'YYYTool/Category/Foundation/NSString'
  
  s.resource_bundles = {
      'BZWWebViewController' => ['BZWWebViewController/Assets/*.png']
  }
end
