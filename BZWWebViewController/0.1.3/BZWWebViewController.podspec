Pod::Spec.new do |s|
  s.name             = 'BZWWebViewController'
  s.version          = '0.1.3'
  s.summary          = 'BZWWebViewController'
  s.description      = <<-DESC
    BZWWebViewController 网页浏览器
    pod 'BZWWebViewController'

                       DESC

  s.homepage         = 'https://gitlab.com/276523923/BZWWebViewController.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }
  s.source           = { :git => 'git@gitlab.com:276523923/BZWWebViewController.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  s.static_framework = true

  s.source_files = 'BZWWebViewController/Classes/**/*'
  s.public_header_files = 'BZWWebViewController/Classes/**/*.h'

  s.dependency 'YYYTool/Helper/YYYCommonMacro'
  s.dependency 'BZWBase/Helper/BZWShare'
  s.dependency 'BZWBase/Helper/BZWConstants'
  s.dependency 'YYYTool/Category/Foundation/NSString'
end
