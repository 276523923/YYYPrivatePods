#
# Be sure to run `pod lib lint YYYHookTool.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'YYYHookTool'
  s.version      = '1.0.1.2'
  s.summary      = 'YYYHookTool'

  s.description  = <<-DESC
                    this is YYYHookTool
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/YYYHookTool.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/YYYHookTool.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  # s.static_framework = true
  s.vendored_frameworks = "YYYHookTool/Classes/YYYTool.framework","YYYHookTool/Classes/XXAcceleratorPlugin.framework"
#  s.vendored_frameworks =

  s.source_files  = "YYYHookTool/Classes/**/*.{h,m}"
  s.public_header_files = "YYYHookTool/Classes/**/*.h"

  # s.resources = "YYYHookTool/Assets/**/*"
  # ss.resource_bundles = {
  #   YYYHookTool => ["YYYHookTool/Assets/**/*"]
  # }

  # s.dependency ""
end
