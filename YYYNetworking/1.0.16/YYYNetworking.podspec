#
# Be sure to run `pod lib lint YYYNetworking.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YYYNetworking'
  s.version          = '1.0.16'
  s.summary          = 'YYYNetworking'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  YYYNetworking 网络请求框架
                       DESC

  s.homepage         = 'https://gitlab.com/276523923/YYYNetworking.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }
  s.source           = { :git => 'git@gitlab.com:276523923/YYYNetworking.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  
  s.subspec 'APIProtocol' do |p|
      p.source_files  = "YYYNetworking/Classes/APIProtocol/*.{m,h}"
      p.public_header_files = "YYYNetworking/Classes/APIProtocol/*.{h}"
  end
  
  s.subspec 'APIReachabilityManager' do |ss|
      ss.dependency 'AFNetworking/Reachability'
      ss.source_files  = "YYYNetworking/Classes/APIReachabilityManager/*.{m,h}"
      ss.public_header_files = "YYYNetworking/Classes/APIReachabilityManager/*.{h}"
  end
  
  s.subspec 'APICache' do |c|
      c.dependency 'YYYCache'
      c.dependency 'YYYNetworking/APIProtocol'
      c.source_files  = "YYYNetworking/Classes/APICache/*.{m,h}"
      c.public_header_files = "YYYNetworking/Classes/APICache/*.{h}"
  end
  
  s.subspec 'APIBase' do |b|
      b.dependency 'AFNetworking/NSURLSession'
      b.dependency 'YYYNetworking/APIProtocol'
      b.dependency 'YYYNetworking/APICache'
      b.dependency 'YYYNetworking/APIReachabilityManager'
      b.source_files  = "YYYNetworking/Classes/APIBase/*.{m,h}"
      b.public_header_files = "YYYNetworking/Classes/APIBase/*.{h}"
  end
  
end
