

Pod::Spec.new do |s|

  s.name         = "AISeedSDK"
  s.version      = "1.32.3"
  s.summary      = "This is a unreal bridge to connect two worlds"

  s.license      = { :type => "Apache-2.0", :file => "LICENSE" }
  s.homepage     = "git@gitlab.com:276523923/AISeedSDK.git"
  s.author       = { "DellMac" => "Customer Service System" }

  s.platform     = :ios
  s.ios.deployment_target = "8.0"
  s.frameworks = "Foundation", "UIKit", "AdSupport", "AudioToolbox", "AVFoundation", "CFNetwork", "CoreAudio", "CoreFoundation", "CoreGraphics", "CoreImage", "CoreMedia", "CoreMotion", "CoreTelephony",
  "CoreText", "CoreVideo", "GameController", "OpenAL", "OpenGLES", "QuartzCore", "MediaPlayer", "MediaPlayer", "SystemConfiguration"
  s.weak_frameworks = "UserNotifications"
  s.libraries = "c++", "iconv", "z", "resolv"
  
  s.source       = { :git => 'git@gitlab.com:276523923/AISeedSDK.git', :tag => s.version.to_s}
  s.source_files = "*.{h}"
  s.requires_arc = true

  s.vendored_libraries = "libAISeedSDK.a"

end


