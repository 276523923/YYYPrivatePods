#
# Be sure to run `pod lib lint YYYTool.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YYYTool'
  s.version          = '1.0.15'
  s.summary          = 'YYYTool'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  YYYTool Category
  
  pod 'YYYTool'
  
  Category
  - pod 'YYYTool/Category'
  
  -- pod 'YYYTool/Category/Foundation'
  --- pod 'YYYTool/Category/Foundation/NSArray'
  --- pod 'YYYTool/Category/Foundation/NSDate'
  --- pod 'YYYTool/Category/Foundation/NSDictionary'
  --- pod 'YYYTool/Category/Foundation/NSFileManager'
  --- pod 'YYYTool/Category/Foundation/NSNumber'
  --- pod 'YYYTool/Category/Foundation/NSObject'
  --- pod 'YYYTool/Category/Foundation/NSOrderedSet'
  --- pod 'YYYTool/Category/Foundation/NSString'
  
  -- pod 'YYYTool/Category/UIKit'
  --- pod 'YYYTool/Category/UIKit/CALayer'
  --- pod 'YYYTool/Category/UIKit/UIApplication'
  --- pod 'YYYTool/Category/UIKit/UICollectionView'
  --- pod 'YYYTool/Category/UIKit/UIDevice'
  --- pod 'YYYTool/Category/UIKit/UIFont'
  --- pod 'YYYTool/Category/UIKit/UIImage'
  --- pod 'YYYTool/Category/UIKit/UIImageView'
  --- pod 'YYYTool/Category/UIKit/UIScrollView'
  --- pod 'YYYTool/Category/UIKit/UITableView'
  --- pod 'YYYTool/Category/UIKit/UITableViewCell'
  --- pod 'YYYTool/Category/UIKit/UICollectionViewCell'
  --- pod 'YYYTool/Category/UIKit/UITextField'
  --- pod 'YYYTool/Category/UIKit/UIView'
  --- pod 'YYYTool/Category/UIKit/UIViewController'
  --- pod 'YYYTool/Category/UIKit/UIWindow'
  
  Helper
  - pod 'YYYTool/Helper'
  -- pod 'YYYTool/Helper/YYYBDLocationCoordinate'
  -- pod 'YYYTool/Helper/YYYCommonMacro'
  -- pod 'YYYTool/Helper/YYYMesssageSend'
  -- pod 'YYYTool/Helper/YYYUtilMethod'

                       DESC

  s.homepage         = 'https://gitlab.com/276523923/YYYTool.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }
  s.source           = { :git => 'git@gitlab.com:276523923/YYYTool.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.requires_arc = true
  
  s.subspec 'Category' do |v|
      # Category 路径
      file_patch = "YYYTool/Classes/Category"
      
      # Foundation
      v.subspec 'Foundation' do |f|
          
          f.subspec 'NSArray' do |ss|
              ss_name = "NSArray"
              ss.source_files = file_patch + "/Foundation/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/Foundation/" +  ss_name + "/*.{h}"
          end
          
          f.subspec 'NSDate' do |ss|
              ss_name = "NSDate"
              ss.source_files = file_patch + "/Foundation/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/Foundation/" +  ss_name + "/*.{h}"
          end
          
          f.subspec 'NSDictionary' do |ss|
              ss_name = "NSDictionary"
              ss.dependency 'YYYTool/Category/Foundation/NSDate'
              ss.source_files = file_patch + "/Foundation/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/Foundation/" +  ss_name + "/*.{h}"
          end
          
          f.subspec 'NSFileManager' do |ss|
              ss_name = "NSFileManager"
              ss.source_files = file_patch + "/Foundation/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/Foundation/" +  ss_name + "/*.{h}"
          end
          
          f.subspec 'NSNumber' do |ss|
              ss_name = "NSNumber"
              ss.source_files = file_patch + "/Foundation/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/Foundation/" +  ss_name + "/*.{h}"
          end
          
          f.subspec 'NSObject' do |ss|
              ss_name = "NSObject"
              ss.source_files = file_patch + "/Foundation/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/Foundation/" +  ss_name + "/*.{h}"
          end
          
          f.subspec 'NSOrderedSet' do |ss|
              ss_name = "NSOrderedSet"
              ss.source_files = file_patch + "/Foundation/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/Foundation/" +  ss_name + "/*.{h}"
          end
          
          f.subspec 'NSString' do |ss|
              ss_name = "NSString"
              ss.source_files = file_patch + "/Foundation/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/Foundation/" +  ss_name + "/*.{h}"
          end
      end
      
      # UIKit
      v.subspec 'UIKit' do |u|
          
          u.subspec 'CALayer' do |ss|
              ss_name = "CALayer"
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UIApplication' do |ss|
              ss_name = "UIApplication"
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UICollectionView' do |ss|
              ss_name = "UICollectionView"
              ss.dependency 'YYYTool/Category/Foundation/NSDictionary'
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UIColor' do |ss|
              ss_name = "UIColor"
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UIDevice' do |ss|
              ss_name = "UIDevice"
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UIFont' do |ss|
              ss_name = "UIFont"
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UIImage' do |ss|
              ss_name = "UIImage"
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UIImageView' do |ss|
              ss_name = "UIImageView"
              ss.dependency 'YYYTool/Category/Foundation/NSObject'
              ss.dependency 'YYYTool/Category/UIKit/UIView'
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UIScrollView' do |ss|
              ss_name = "UIScrollView"
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UITableView' do |ss|
              ss_name = "UITableView"
              ss.dependency 'YYYTool/Category/Foundation/NSDictionary'
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UITableViewCell' do |ss|
              ss_name = "UITableViewCell"
              ss.dependency 'YYYTool/Category/Foundation/NSObject'
              ss.dependency 'YYYTool/Category/UIKit/UIView'
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UICollectionViewCell' do |ss|
              ss_name = "UICollectionViewCell"
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UITextField' do |ss|
              ss_name = "UITextField"
              ss.dependency 'YYYTool/Category/Foundation/NSObject'
              ss.dependency 'YYYTool/Category/Foundation/NSString'
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UIView' do |ss|
              ss_name = "UIView"
              ss.dependency 'YYYTool/Category/Foundation/NSObject'
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UIViewController' do |ss|
              ss_name = "UIViewController"
              ss.dependency 'YYYTool/Category/Foundation/NSObject'
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
          u.subspec 'UIWindow' do |ss|
              ss_name = "UIWindow"
              ss.source_files = file_patch  + "/UIKit/" + ss_name + "/*.{m,h}"
              ss.public_header_files = file_patch  + "/UIKit/" + ss_name + "/*.{h}"
          end
          
      end
  end
  
  ### Helper 组件
  s.subspec 'Helper' do |v|
      # Helper 路径
      file_patch = "YYYTool/Classes/Helper/"
      # 子组件
      v.subspec 'YYYBDLocationCoordinate' do |ss|
          ss_name = "YYYBDLocationCoordinate"
          ss.source_files = file_patch + ss_name + "/*.{m,h}"
          ss.public_header_files = file_patch + ss_name + "/*.{h}"
      end
      
      v.subspec 'YYYCommonMacro' do |ss|
          ss_name = "YYYCommonMacro"
          ss.dependency 'YYYTool/Helper/YYYUtilMethod'
          ss.source_files = file_patch + ss_name + "/*.{m,h}"
          ss.public_header_files = file_patch + ss_name + "/*.{h}"
      end
      
      v.subspec 'YYYMesssageSend' do |ss|
          ss_name = "YYYMesssageSend"
          ss.source_files = file_patch + ss_name + "/*.{m,h}"
          ss.public_header_files = file_patch + ss_name + "/*.{h}"
      end
      
      v.subspec 'YYYUtilMethod' do |ss|
          ss_name = "YYYUtilMethod"
          ss.dependency 'YYYTool/Category/UIKit/UIDevice'
          ss.dependency 'YYYTool/Category/Foundation/NSDate'
          ss.source_files = file_patch + ss_name + "/*.{m,h}"
          ss.public_header_files = file_patch + ss_name + "/*.{h}"
      end
  end

end
