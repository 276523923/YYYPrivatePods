#
# Be sure to run `pod lib lint BZWMaskWindowView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BZWMaskWindowView'
  s.version          = '0.1.0'
  s.summary          = 'BZWMaskWindowView 遮罩窗口'

  s.description      = <<-DESC
全屏遮罩窗口控件 BZWMaskWindowView
                       DESC

  s.homepage         = 'https://yyyue@bitbucket.org/yyyue/bzwmaskwindowview.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }
  s.source           = { :git => 'https://yyyue@bitbucket.org/yyyue/bzwmaskwindowview.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  s.source_files = 'BZWMaskWindowView/Classes/**/*'

end
