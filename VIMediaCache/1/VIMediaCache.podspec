#
# Be sure to run `pod lib lint VIMediaCache.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'VIMediaCache'
  s.version      = '1'
  s.summary      = 'VIMediaCache'

  s.description  = <<-DESC
                    this is VIMediaCache
                   DESC

  s.homepage         = 'https://yyyue@bitbucket.org/yyyue/vimediacache.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'https://yyyue@bitbucket.org/yyyue/vimediacache.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  # s.static_framework = true

  s.source_files  = "VIMediaCache/Classes/**/*.{h,m}"
  s.public_header_files = "VIMediaCache/Classes/**/*.h"

  # s.resources = "VIMediaCache/Assets/**/*"
  # ss.resource_bundles = {
  #   VIMediaCache => ["VIMediaCache/Assets/**/*"]
  # }

  s.dependency "YYYTool/Category/Foundation/NSString"
end
