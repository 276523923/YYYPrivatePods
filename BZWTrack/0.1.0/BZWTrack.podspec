#
# Be sure to run `pod lib lint BZWTrack.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BZWTrack'
  s.version          = '0.1.0'
  s.summary          = 'BZWTrack 统计.'

  s.description      = <<-DESC
BZWTrack 统计类，使用MTA统计跟友盟统计
                       DESC

  s.homepage         = 'https://yyyue@bitbucket.org/yyyue/bzwtrack.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }
  s.source           = { :git => 'https://yyyue@bitbucket.org/yyyue/bzwtrack.git', :tag => s.version.to_s }
  s.static_framework = true
  s.ios.deployment_target = '8.0'
  s.dependency 'QQ_MTA'
  s.dependency 'UMCCommon'
  s.dependency 'UMCSecurityPlugins'
  s.dependency 'UMCAnalytics'
#s.frameworks = 'UTDID','SecurityEnvSDK'
  s.source_files = 'BZWTrack/Classes/**/*'


s.pod_target_xcconfig = {
'OTHER_LDFLAGS' => '$(inherited) -ObjC'
}

end
