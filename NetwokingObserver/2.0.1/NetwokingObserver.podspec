#
# Be sure to run `pod lib lint NetwokingObserver.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'NetwokingObserver'
  s.version      = '2.0.1'
  s.summary      = 'NetwokingObserver'

  s.description  = <<-DESC
                    this is NetwokingObserver
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/NetwokingObserver.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/NetwokingObserver.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  # s.static_framework = true

  s.source_files  = "NetwokingObserver/Classes/**/*.{h,m}"
  s.public_header_files = "NetwokingObserver/Classes/**/*.h"

  # s.resources = "NetwokingObserver/Assets/**/*"
  # ss.resource_bundles = {
  #   NetwokingObserver => ["NetwokingObserver/Assets/**/*"]
  # }

  # s.dependency ""
end
