#
# Be sure to run `pod lib lint YYPay.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'YYPay'
  s.version      = '2.0.0'
  s.summary      = 'YYPay'

  s.description  = <<-DESC
                    this is YYPay
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/YYPay.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/YYPay.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  s.static_framework = true

  s.source_files  = "YYPay/Classes/**/*.{h,m}"
  s.public_header_files = "YYPay/Classes/**/*.h"

  # s.resources = "YYPay/Assets/**/*"
  # ss.resource_bundles = {
  #   YYPay => ["YYPay/Assets/**/*"]
  # }

  s.dependency "Ads"
end
