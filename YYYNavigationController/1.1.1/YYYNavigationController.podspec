#
# Be sure to run `pod lib lint YYYNavigationController.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'YYYNavigationController'
  s.version      = '1.1.1'
  s.summary      = 'YYYNavigationController'

  s.description  = <<-DESC
                    this is YYYNavigationController
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/YYYNavigationController.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/YYYNavigationController.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  # s.static_framework = true

  s.source_files  = "YYYNavigationController/Classes/**/*.{h,m}"
  s.public_header_files = "YYYNavigationController/Classes/**/*.h"

  # s.resources = "YYYNavigationController/Assets/**/*"
  # ss.resource_bundles = {
  #   YYYNavigationController => ["YYYNavigationController/Assets/**/*"]
  # }

  # s.dependency ""
end
