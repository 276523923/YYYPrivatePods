#
# Be sure to run `pod lib lint YYYSafe.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'YYYSafe'
  s.version      = '2.0.0'
  s.summary      = 'YYYSafe'

  s.description  = <<-DESC
                    this is YYYSafe
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/YYYSafe.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/YYYSafe.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  # s.static_framework = true

  s.source_files  = "YYYSafe/Classes/**/*.{h,m}"
  s.public_header_files = "YYYSafe/Classes/**/*.h"

  # s.resources = "YYYSafe/Assets/**/*"
  # ss.resource_bundles = {
  #   YYYSafe => ["YYYSafe/Assets/**/*"]
  # }

   s.dependency "YYYKeyChain"
end
