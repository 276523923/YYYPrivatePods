#
# Be sure to run `pod lib lint BZWShare.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|

    s.name         = 'BZWShare'
    s.version      = '1.0.0'
    s.summary      = 'BZWShare'

    s.description  = <<-DESC
    this is BZWShare
    DESC

    s.homepage         = 'https://gitlab.com/276523923/BZWShare.git'
    s.license          = { :type => "MIT", :file => "LICENSE" }
    s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

    s.source       = { :git => 'git@gitlab.com:276523923/BZWShare.git', :tag => s.version.to_s }
    s.ios.deployment_target = '8.0'
    s.requires_arc = true

    s.static_framework = true

    s.source_files  = "BZWShare/Classes/**/*.{h,m}"
    s.public_header_files = "BZWShare/Classes/**/*.h"

    s.resource_bundles = {
      "BZWShare" => ["BZWShare/Assets/**/*"]
    }

    s.dependency 'BZWBase/View/BZWMaskWindowView'
    s.dependency 'BZWBase/Helper/BZWConstants'
    s.dependency 'BZWBase/Helper/BZWProgressHUD'

    s.dependency 'YYYTool/Helper/YYYUtilMethod'
    s.dependency 'YYYTool/Category/UIKit/UIView'
    s.dependency 'YYYTool/Category/UIKit/UIWindow'
    s.dependency 'YYYTool/Category/Foundation/NSDictionary'

    s.dependency 'UMCCommon'
    s.dependency 'UMCShare/UI'
    s.dependency 'UMCShare/Social/ReducedWeChat'
    s.dependency 'UMCShare/Social/ReducedQQ'
    s.dependency 'UMCShare/Social/ReducedSina'
    s.dependency 'Masonry'

end
