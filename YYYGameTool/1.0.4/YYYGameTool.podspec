#
# Be sure to run `pod lib lint YYYGameTool.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'YYYGameTool'
  s.version      = '1.0.4'
  s.summary      = 'YYYGameTool'

  s.description  = <<-DESC
                    this is YYYGameTool
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/YYYGameTool.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/YYYGameTool.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  s.static_framework = true

  s.source_files  = "YYYGameTool/Classes/**/*.{h,m}"
  s.public_header_files = "YYYGameTool/Classes/**/*.h"

  # s.resources = "YYSpeed/Assets/**/*"
  
  s.subspec 'YYYPay' do |ss|
      ss_name = "YYYPay"
      ss.source_files = "YYYGameTool/Classes/" + ss_name + "/*"
      ss.public_header_files = "YYYGameTool/Classes/" + ss_name + "/*.{h}"
      ss.vendored_frameworks = "YYYGameTool/Classes/XXAcceleratorPlugin.framework"
      ss.resource_bundles = {
        'YYSpeed' => ["YYYGameTool/Assets/**/*"]
      }
  end

  s.subspec 'YYYBackUpHelper' do |ss|
      ss_name = "YYYBackUpHelper"
      ss.dependency "SSZipArchive"
      ss.dependency "YYYProgressHUD"
      ss.source_files = "YYYGameTool/Classes/" + ss_name + "/*"
      ss.public_header_files = "YYYGameTool/Classes/" + ss_name + "/*.{h}"
  end

  s.subspec 'mem' do |ss|
      ss_name = "mem"
      ss.source_files = "YYYGameTool/Classes/" + ss_name + "/*"
      ss.public_header_files = "YYYGameTool/Classes/" + ss_name + "/*.{h}"
  end

  s.subspec 'memui' do |ss|
      ss_name = "memui"
      ss.dependency 'YYYGameTool/mem'
      ss.source_files = "YYYGameTool/Classes/" + ss_name + "/*.{m,h}"
      ss.public_header_files = "YYYGameTool/Classes/" + ss_name + "/*.{h}"
  end
  
  s.subspec 'YYYFakeTouch' do |ss|
      ss_name = "YYYFakeTouch"
      ss.source_files = "YYYGameTool/Classes/" + ss_name + "/*.{m,h}"
      ss.public_header_files = "YYYGameTool/Classes/" + ss_name + "/*.{h}"
  end


  # s.resources = "YYYGameTool/Assets/**/*"
  # ss.resource_bundles = {
  #   YYYGameTool => ["YYYGameTool/Assets/**/*"]
  # }

  # s.dependency ""
end
