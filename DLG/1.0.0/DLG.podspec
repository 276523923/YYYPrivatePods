#
# Be sure to run `pod lib lint DLG.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'DLG'
  s.version      = '1.0.0'
  s.summary      = 'DLG'

  s.description  = <<-DESC
                    this is DLG
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/DLG.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/DLG.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true
  s.static_framework = true

  s.subspec 'mem' do |ss|
      ss_name = "mem"
      ss.source_files = "DLG/Classes/" + ss_name + "/*"
      ss.public_header_files = "DLG/Classes/" + ss_name + "/*.{h}"
  end

  s.subspec 'memui' do |ss|
      ss_name = "memui"
      ss.dependency 'DLG/mem'
      ss.source_files = "DLG/Classes/" + ss_name + "/*.{m,h}"
      ss.public_header_files = "DLG/Classes/" + ss_name + "/*.{h}"
  end
  

#  s.source_files  = "DLG/Classes/**/*.{h,m}"
#  s.public_header_files = "DLG/Classes/**/*.h"

  # s.resources = "DLG/Assets/**/*"
  # ss.resource_bundles = {
  #   DLG => ["DLG/Assets/**/*"]
  # }

  # s.dependency ""
end
