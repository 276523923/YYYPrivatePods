#
# Be sure to run `pod lib lint Ads.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'Ads'
  s.version      = '2.0.3'
  s.summary      = 'Ads'

  s.description  = <<-DESC
                    this is Ads
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/Ads.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/Ads.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

   s.static_framework = true

  s.source_files  = "Ads/Classes/**/*.{h,m}"
  s.public_header_files = "Ads/Classes/**/*.h"

  # s.resources = "Ads/Assets/**/*"
  # ss.resource_bundles = {
  #   Ads => ["Ads/Assets/**/*"]
  # }

  s.dependency "Google-Mobile-Ads-SDK",'7.37.0'
end
