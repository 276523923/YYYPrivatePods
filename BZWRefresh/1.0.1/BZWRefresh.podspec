#
# Be sure to run `pod lib lint BZWRefresh.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'BZWRefresh'
  s.version      = '1.0.1'
  s.summary      = 'BZWRefresh'

  s.description  = <<-DESC
                    this is BZWRefresh
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/BZWRefresh.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/BZWRefresh.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true
  s.static_framework = true

  s.dependency 'YYYTool/Helper/YYYCommonMacro'
  s.dependency 'YYYTool/Category/UIKit/UIView'
  s.dependency 'YYYTool/Category/UIKit/UIScrollView'

  s.source_files  = "BZWRefresh/Classes/**/*.{h,m}"
  s.public_header_files = "BZWRefresh/Classes/**/*.h"

  s.resource_bundles = {
     'BZWRefresh' => ["BZWRefresh/Assets/**/*"]
  }

end
