#
# Be sure to run `pod lib lint YYYCalendarHelper.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YYYCalendarHelper'
  s.version          = '1.0.1'
  s.summary          = '日期获取农历、黄历信息'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  
  通过日期,转换成对应的农历、黄历信息
  每日宜忌
  冲煞
  胎神
  五行
  吉神宜趋
  凶神宜忌
  诸神方位：财神、喜神、福神
  彭祖百忌
  每天的时辰凶吉
  十二元神
  建除十二神
                       DESC

  s.homepage         = 'https://gitlab.com/276523923/YYYCalendarHelper'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }
  s.source           = { :git => 'git@gitlab.com:276523923/YYYCalendarHelper.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'
  s.static_framework = true
  s.requires_arc = true

  s.source_files = 'YYYCalendarHelper/Classes/**/*'
  s.resources = 'YYYCalendarHelper/Assets/*'
  
#  s.resource_bundles = {
#      'YYYCalendarHelper' => ['YYYCalendarHelper/Assets/*.png']
#  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
