Pod::Spec.new do |s|
  s.name             = 'BZWBaseModel'
  s.version          = '0.1.0'
  s.summary          = 'BZWBaseModel'

  s.description      = <<-DESC

基础模型 BZWBaseModel

                       DESC

  s.homepage         = 'https://yyyue@bitbucket.org/yyyue/bzwbasemodel.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }
  s.source           = { :git => 'https://yyyue@bitbucket.org/yyyue/bzwbasemodel.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  s.dependency 'YYModel'
  s.source_files = 'BZWBaseModel/Classes/**/*'
  
end
