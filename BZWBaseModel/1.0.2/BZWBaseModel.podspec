#
# Be sure to run `pod lib lint BZWBaseModel.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'BZWBaseModel'
  s.version      = '1.0.2'
  s.summary      = 'BZWBaseModel'

  s.description  = <<-DESC
                    this is BZWBaseModel
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/BZWBaseModel.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/BZWBaseModel.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  # s.static_framework = true

  s.source_files  = "BZWBaseModel/Classes/**/*.{h,m}"
  s.public_header_files = "BZWBaseModel/Classes/**/*.h"

  # s.resources = "BZWBaseModel/Assets/**/*"
  # ss.resource_bundles = {
  #   BZWBaseModel => ["BZWBaseModel/Assets/**/*"]
  # }

  s.dependency "YYModel"
end
