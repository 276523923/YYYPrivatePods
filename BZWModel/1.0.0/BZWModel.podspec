#
# Be sure to run `pod lib lint BZWModel.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'BZWModel'
  s.version      = '1.0.0'
  s.summary      = 'BZWModel'

  s.description  = <<-DESC
                    this is BZWModel
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/BZWModel.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/BZWModel.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  s.static_framework = true

  s.source_files  = "BZWModel/Classes/**/*.{h,m}"
  s.public_header_files = "BZWModel/Classes/**/*.h"

  # s.resources = "BZWModel/Assets/**/*"
  # ss.resource_bundles = {
  #   BZWModel => ["BZWModel/Assets/**/*"]
  # }

  s.dependency "BZWBaseModel"
  file_patch = "BZWModel/Classes/"
  # view_soureces_patch = "BZWBase/Assets/View/"
  
  
  s.subspec 'Enterprise' do |ss|
      ss_name = "Enterprise"
      ss.dependency "YYYTool/Category/Foundation/NSDictionary"
      ss.dependency "YYYTool/Category/Foundation/NSDate"
      ss.dependency "YYYTool/Helper/YYYBDLocationCoordinate"
      ss.dependency "BZWBase/Helper/BZWConstants"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'Activity' do |ss|
      ss_name = "Activity"
      ss.dependency "BZWModel/Enterprise"
      ss.dependency "BZWModel/Insurance"
      ss.dependency "YYYTool/Helper/YYYBDLocationCoordinate"
      ss.dependency "YYYTool/Category/Foundation/NSDictionary"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'Advert' do |ss|
      ss_name = "Advert"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'App' do |ss|
      ss_name = "App"
      ss.dependency "BZWModel/Enterprise"
      ss.dependency "BZWModel/Advert"
      ss.dependency "BZWModel/Case"
      ss.dependency "BZWModel/News"
      ss.dependency "BZWModel/Video"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'Case' do |ss|
      ss_name = "Case"
      ss.dependency "BZWModel/Enterprise"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'CaseClassList' do |ss|
      ss_name = "CaseClassList"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'Designer' do |ss|
      ss_name = "Designer"
      ss.dependency "YYYTool/Category/Foundation/NSDictionary"
      ss.dependency "YYYTool/Category/Foundation/NSDate"
      ss.dependency "BZWModel/Case"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'Comment' do |ss|
      ss_name = "Comment"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'GlobalData' do |ss|
      ss_name = "GlobalData"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'Insurance' do |ss|
      ss_name = "Insurance"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'Message' do |ss|
      ss_name = "Message"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'News' do |ss|
      ss_name = "News"
      ss.dependency "BZWModel/Enterprise"
      ss.dependency "YYYTool/Category/Foundation/NSDictionary"
      ss.dependency "YYYTool/Category/Foundation/NSDate"
      ss.dependency "YYYTool/Helper/YYYUtilMethod"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'Response' do |ss|
      ss_name = "Response"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'SystemRecommend' do |ss|
      ss_name = "SystemRecommend"
      ss.dependency "BZWModel/News"
      ss.dependency "BZWModel/Case"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  s.subspec 'Video' do |ss|
      ss_name = "Video"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
  end
  
  
end
