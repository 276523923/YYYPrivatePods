#
# Be sure to run `pod lib lint YYYHookToolNAD.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'YYYHookToolNAD'
  s.version      = '1.0.5'
  s.summary      = 'YYYHookToolNAD'

  s.description  = <<-DESC
                    this is YYYHookToolNAD
                   DESC

  s.homepage         = 'https://git.code.tencent.com/276523923/YYYHookToolNAD.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'https://git.code.tencent.com/276523923/YYYHookToolNAD.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  # s.static_framework = true

  s.source_files  = "YYYHookToolNAD/Classes/**/*.{h,m}"
  s.public_header_files = "YYYHookToolNAD/Classes/**/*.h"
  s.vendored_frameworks = "YYYHookToolNAD/Classes/YYYTool-NAD.framework"

  # s.resources = "YYYHookToolNAD/Assets/**/*"
  # ss.resource_bundles = {
  #   YYYHookToolNAD => ["YYYHookToolNAD/Assets/**/*"]
  # }

  # s.dependency ""
end
