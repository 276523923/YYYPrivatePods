#
# Be sure to run `pod lib lint BZWPageViewController.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'BZWPageViewController'
  s.version      = '1.0.0'
  s.summary      = 'BZWPageViewController'

  s.description  = <<-DESC
                    this is BZWPageViewController
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/BZWPageViewController.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/BZWPageViewController.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  # s.static_framework = true

  s.source_files  = "BZWPageViewController/Classes/**/*.{h,m}"
  s.public_header_files = "BZWPageViewController/Classes/**/*.h"

  # s.resources = "BZWPageViewController/Assets/**/*"
  # ss.resource_bundles = {
  #   BZWPageViewController => ["BZWPageViewController/Assets/**/*"]
  # }
  s.dependency "YYYWeakProxy"
  
end
