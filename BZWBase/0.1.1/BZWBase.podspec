#
# Be sure to run `pod lib lint BZWBase.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BZWBase'
  s.version          = '0.1.1'
  s.summary          = '保障网基础工具合集'


  s.description      = <<-DESC

保障网App 基础工具合集
pod 'BZWBase'

- pod 'BZWBase/View'
-- pod 'BZWBase/View/BZWMaskWindowView'

- pod 'BZWBase/Helper'
-- pod 'BZWBase/Helper/BZWConstants'
-- pod 'BZWBase/Helper/BZWTrack'
-- pod 'BZWBase/Helper/BZWLocation'

                       DESC

  s.homepage         = 'https://yyyue@bitbucket.org/yyyue/bzwbase.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }
  s.source           = { :git => 'https://yyyue@bitbucket.org/yyyue/bzwbase.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.static_framework = true

  ### View 组件
  s.subspec 'View' do |v|
    # View 路径
    file_patch = "BZWBase/Classes/View/"
    # 子组件 
    v.subspec 'BZWMaskWindowView' do |ss|
      ss_name = "BZWMaskWindowView"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
    end
  end

  ### Helper 组件
  s.subspec 'Helper' do |h|
    # Helper 路径
    file_patch = "BZWBase/Classes/Helper/"

    # BZWConstants
    h.subspec 'BZWConstants' do |ss|
      ss_name = "BZWConstants"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
    end

    # BZWTrack
    h.subspec 'BZWTrack' do |ss|  
      ss_name = "BZWTrack"
      ss.dependency 'QQ_MTA'
      ss.dependency 'UMCCommon'
      ss.dependency 'UMCSecurityPlugins'
      ss.dependency 'UMCAnalytics'
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
    end

    # BZWLocation
    h.subspec 'BZWLocation' do |ss|  
      ss_name = "BZWLocation"
      ss.source_files = file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = file_patch + ss_name + "/*.{h}"
    end

  end
end
