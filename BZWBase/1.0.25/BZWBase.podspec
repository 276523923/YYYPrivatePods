#
# Be sure to run `pod lib lint BZWBase.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BZWBase'
  s.version          = '1.0.25'
  s.summary          = '保障网基础工具合集'

  s.description      = <<-DESC

保障网App 基础工具合集
pod 'BZWBase'

- pod 'BZWBase/View'
-- pod 'BZWBase/View/BZWMaskWindowView'
-- pod 'BZWBase/View/BZWContactServiceView'
-- pod 'BZWBase/View/BZWTextView'
-- pod 'BZWBase/View/BZWGradientLayerView'
-- pod 'BZWBase/View/BZWNavigationBar'
-- pod 'BZWBase/View/BZWLogoBorderImageView'
-- pod 'BZWBase/View/BZWPageControl'
-- pod 'BZWBase/View/BZWTabHeaderView'
-- pod 'BZWBase/View/BZWTabSegmentedView'

- pod 'BZWBase/Helper'
-- pod 'BZWBase/Helper/BZWConstants'
-- pod 'BZWBase/Helper/BZWTrack'
-- pod 'BZWBase/Helper/BZWLocation'
-- pod 'BZWBase/Helper/BZWProgressHUD'
-- pod 'BZWBase/Helper/BZWShare'
-- pod 'BZWBase/Helper/BZWRefresh'
-- pod 'BZWBase/Helper/BZWFetchedResultsControllerDelegate'

- pod 'BZWBase/Category'


                       DESC

  s.homepage         = 'https://gitlab.com/276523923/BZWBase.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }
  s.source           = { :git => 'git@gitlab.com:276523923/BZWBase.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.static_framework = true

  ### View 组件
  s.subspec 'View' do |v|
    # View 路径
    view_file_patch = "BZWBase/Classes/View/"
    view_soureces_patch = "BZWBase/Assets/View/"
    
    # BZWMaskWindowView
    v.subspec 'BZWMaskWindowView' do |ss|
      ss_name = "BZWMaskWindowView"
      ss.source_files = view_file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = view_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWContactServiceView
    v.subspec 'BZWContactServiceView' do |ss|
        ss_name = "BZWContactServiceView"
        ss.dependency 'Masonry'
        ss.dependency 'BZWBase/Helper/BZWConstants'
        ss.dependency 'BZWBase/View/BZWMaskWindowView'
        ss.dependency 'YYYTool/Category/UIKit/UIApplication'
        ss.dependency 'YYYTool/Category/UIKit/UIView'
        ss.dependency 'YYYTool/Helper/YYYUtilMethod'
        ss.resource_bundles = {
            ss_name => [view_soureces_patch + ss_name + "/*.png"]
        }
        ss.source_files = view_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = view_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWTextView
    v.subspec 'BZWTextView' do |ss|
        ss_name = "BZWTextView"
        ss.source_files = view_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = view_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWGradientLayerView
    v.subspec 'BZWGradientLayerView' do |ss|
        ss_name = "BZWGradientLayerView"
        ss.source_files = view_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = view_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWNavigationBar
    v.subspec 'BZWNavigationBar' do |ss|
        ss_name = "BZWNavigationBar"
        ss.dependency 'BZWBase/Helper/BZWConstants'
        ss.source_files = view_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = view_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWLogoBorderImageView
    v.subspec 'BZWLogoBorderImageView' do |ss|
        ss_name = "BZWLogoBorderImageView"
        ss.dependency 'BZWBase/Helper/BZWConstants'
        ss.source_files = view_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = view_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWPageControl
    v.subspec 'BZWPageControl' do |ss|
        ss_name = "BZWPageControl"
        ss.source_files = view_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = view_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWTabHeaderView
    v.subspec 'BZWTabHeaderView' do |ss|
        ss_name = "BZWTabHeaderView"
        ss.dependency 'Masonry'
        ss.dependency 'BZWBase/Helper/BZWConstants'
        ss.dependency 'YYYTool/Helper/YYYUtilMethod'
        ss.source_files = view_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = view_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWTabSegmentedView
    v.subspec 'BZWTabSegmentedView' do |ss|
        ss_name = "BZWTabSegmentedView"
        ss.dependency 'BZWBase/Helper/BZWConstants'
        ss.dependency 'YYYTool/Category/UIKit/UIView'
        ss.dependency 'YYYTool/Category/UIKit/UIScrollView'
        ss.dependency 'YYYTool/Helper/YYYUtilMethod'
        ss.source_files = view_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = view_file_patch + ss_name + "/*.{h}"
    end
  end

  ### Helper 组件
  s.subspec 'Helper' do |h|
    # Helper 路径
    helper_file_patch = "BZWBase/Classes/Helper/"
    helper_soureces_patch = "BZWBase/Assets/Helper/"
    
    # BZWConstants
    h.subspec 'BZWConstants' do |ss|
      ss_name = "BZWConstants"
#      ss.resources = helper_soureces_patch + ss_name + "/*.xcassets"
      ss.resource_bundles = {
          ss_name => [helper_soureces_patch + ss_name + "/*.png"]
      }
      ss.source_files = helper_file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = helper_file_patch + ss_name + "/*.{h}"
    end

    # BZWTrack
    h.subspec 'BZWTrack' do |ss|  
      ss_name = "BZWTrack"
      ss.dependency 'QQ_MTA'
      ss.dependency 'UMCCommon'
      ss.dependency 'UMCAnalytics'
      ss.source_files = helper_file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = helper_file_patch + ss_name + "/*.{h}"
    end

    # BZWLocation
    h.subspec 'BZWLocation' do |ss|  
      ss_name = "BZWLocation"
      ss.source_files = helper_file_patch + ss_name + "/*.{m,h}"
      ss.public_header_files = helper_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWProgressHUD
    h.subspec 'BZWProgressHUD' do |ss|
        ss_name = "BZWProgressHUD"
        ss.dependency 'BZWBase/Helper/BZWConstants'
        ss.dependency 'YYYTool/Helper/YYYCommonMacro'
#        ss.resources = helper_soureces_patch + ss_name + "/*.xcassets"
        ss.resource_bundles = {
            ss_name => [helper_soureces_patch + ss_name + "/*.png"]
        }
        ss.source_files = helper_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = helper_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWShare
    h.subspec 'BZWShare' do |ss|
        ss_name = "BZWShare"
        
        ss.dependency 'BZWBase/View/BZWMaskWindowView'
        ss.dependency 'BZWBase/Helper/BZWConstants'
        ss.dependency 'BZWBase/Helper/BZWProgressHUD'
        
        ss.dependency 'YYYTool/Helper/YYYUtilMethod'
        ss.dependency 'YYYTool/Category/UIKit/UIView'
        ss.dependency 'YYYTool/Category/UIKit/UIWindow'
        ss.dependency 'YYYTool/Category/Foundation/NSDictionary'
        
        ss.dependency 'UMCCommon'
        ss.dependency 'UMCShare/UI'
        ss.dependency 'UMCShare/Social/ReducedWeChat'
        ss.dependency 'UMCShare/Social/ReducedQQ'
        ss.dependency 'UMCShare/Social/ReducedSina'
        ss.dependency 'Masonry'
#        ss.resources = helper_soureces_patch + ss_name + "/*.png"
        ss.resource_bundles = {
            ss_name => [helper_soureces_patch + ss_name + "/*.png"]
        }
        ss.source_files = helper_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = helper_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWRefresh
    h.subspec 'BZWRefresh' do |ss|
        ss_name = "BZWRefresh"
        ss.dependency 'BZWBase/Helper/BZWConstants'
        ss.dependency 'YYYTool/Helper/YYYCommonMacro'
        ss.dependency 'YYYTool/Category/UIKit/UIView'
        ss.dependency 'YYYTool/Category/UIKit/UIScrollView'
        #        ss.resources = helper_soureces_patch + ss_name + "/*.png"
        ss.resource_bundles = {
            ss_name => [helper_soureces_patch + ss_name + "/*.png"]
        }
        ss.source_files = helper_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = helper_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWFetchedResultsControllerDelegate
    h.subspec 'BZWFetchedResultsControllerDelegate' do |ss|
        ss_name = "BZWFetchedResultsControllerDelegate"
        ss.dependency 'YYYTool/Category/UIKit/UIDevice'
        ss.source_files = helper_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = helper_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWRequestParamKey
    h.subspec 'BZWRequestParamKey' do |ss|
        ss_name = "BZWRequestParamKey"
        ss.source_files = helper_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = helper_file_patch + ss_name + "/*.{h}"
    end
    
    # BZWCellHeightCache
    h.subspec 'BZWCellHeightCache' do |ss|
        ss_name = "BZWCellHeightCache"
        ss.dependency 'YYCache'
        ss.dependency 'YYYTool/Category/UIKit/UITableViewCell'
        ss.dependency 'YYYTool/Category/UIKit/UICollectionViewCell'
        ss.source_files = helper_file_patch + ss_name + "/*.{m,h}"
        ss.public_header_files = helper_file_patch + ss_name + "/*.{h}"
    end
  end
  
  ### Category 组件
  s.subspec 'Category' do |ss|
      # Category 路径
      category_file_patch = "BZWBase/Classes/Category"
      category_soureces_patch = "BZWBase/Assets/Category"
      ss.dependency 'YYText'
      ss.dependency 'YYWebImage'
      ss.dependency 'YYYTool/Category/Foundation/NSObject'
      ss.dependency 'YYYTool/Category/Foundation/NSString'
      ss.dependency 'YYYTool/Category/UIKit/UIView'
      ss.dependency 'YYYTool/Helper/YYYCommonMacro'
      ss.dependency 'BZWBase/Helper/BZWCellHeightCache'
      ss.dependency 'BZWBase/Helper/BZWConstants'
      ss.dependency 'BZWBase/Helper/BZWProgressHUD'
      ss.dependency 'BZWBase/Helper/BZWTrack'
      ss.dependency 'BZWBase/View/BZWNavigationBar'
      ss.source_files = category_file_patch + "/*.{m,h}"
      ss.public_header_files = category_file_patch + "/*.{h}"
  end
  
end

