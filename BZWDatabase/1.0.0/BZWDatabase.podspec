#
# Be sure to run `pod lib lint BZWDatabase.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name         = 'BZWDatabase'
  s.version      = '1.0.0'
  s.summary      = 'BZWDatabase'

  s.description  = <<-DESC
                    this is BZWDatabase
                   DESC

  s.homepage         = 'https://gitlab.com/276523923/BZWDatabase.git'
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author           = { '276523923@qq.com' => 'yyyue@vip.qq.com' }

  s.source       = { :git => 'git@gitlab.com:276523923/BZWDatabase.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  # s.static_framework = true

  s.source_files  = "BZWDatabase/Classes/**/*.{h,m,xcdatamodeld}"
  s.public_header_files = "BZWDatabase/Classes/**/*.h"

  # s.resources = "BZWDatabase/Assets/**/*"
  s.resource_bundles = {
    "BZWDatabase" => ["BZWDatabase/Assets/**/*"]
  }

  s.dependency "YYYTool/Category/Foundation/NSDictionary"
  s.dependency "YYYTool/Category/Foundation/NSArray"
  s.dependency "YYYTool/Category/Foundation/NSString"
  s.dependency "YYYTool/Category/Foundation/NSDate"
  s.dependency "MagicalRecord"
end
